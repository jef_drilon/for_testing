#!/usr/bin/ruby
require './input_functions'
require 'ffi'

require 'json'
#================================for input type

@music_player = [ {:artist => "Van Halen",           :song=>"jump"},
                 {:artist => "Enya",                :song => "Orinoco Flow"},
                 {:artist => "Enya",                :song => "Orinoco Flow"},
                 {:artist => "Justin Clarke",       :song=>"Dinosour Stomp"},
                 {:artist => "Simian Mobile Disco", :song=> "Sleep Deprivation"},
                 {:artist => "Sleep Deprivation",   :song=>"Panama"}
                       
                ]

#===========================computation
def main_menu_albums
    finished = false
    begin
    puts 'Main Menu:'
    puts '1 - Playlist'
    puts '2 - Random'
    puts '3 - Search From the Album'
    puts '4 - Play Songs'
    choice = read_integer("Enter Number")
        case choice
        when 1
           puts " no. |  Song | Artist"
           @music_player.each_with_index do |m, i|
            puts " #{i + 1} |   #{m[:song]} - #{m[:artist]} "
           end
        when 2
            new_list = []
            same_artist = []
            new_data_list = []
            same_artist_list = []
            
           puts " no. |  Song | Artist"
           @music_player.each do |ml|
            artist =  ml[:artist]
            if new_list.last == artist
              
              same_artist << artist
              same_artist_list  << ml
            else
              new_list << artist
              new_data_list  << ml
            end
            
           end
           random_artist =  new_data_list << same_artist_list
           puts random_artist
        when 3
            #artist =  read_string("Enter Artist:")
            songs  =  read_string("Enter Songs:")
            
            @music_player.each do |m|
              if m[:song] == songs
                  puts "Now Playing: #{m[:song]} by: #{m[:artist]}"
              
              end
            end
        when 4
           @music_player.each_with_index do |m, i|
            puts " #{i + 1} |   #{m[:song]} - #{m[:artist]} "
           end
          
          
        else

            puts 'Please select again'
        end
    end until finished
end

def main
    main_menu_albums
end

main
