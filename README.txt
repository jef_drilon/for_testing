__Programming Language__

* Ruby



__Input Files__

Your program are read in a Ruby file at a path specified relevant to the location of the `run.rb` script.


Example JSON input (per the supplied `test-data/01-input.txt` file):


__Action__

Select number 1 to 4
1 for playlist list
2 for Random that not play same artist one after another 
3 Search for songs
4 Play songs

